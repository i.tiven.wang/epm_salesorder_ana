jQuery.sap.declare("mi.test.EPM_SalesOrder_ANA.Component");
sap.ui.getCore().loadLibrary("sap.ui.generic.app");
jQuery.sap.require("sap.ui.generic.app.AppComponent");

sap.ui.generic.app.AppComponent.extend("mi.test.EPM_SalesOrder_ANA.Component", {
	metadata: {
		"manifest": "json"
	}
});